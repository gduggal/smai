#!/usr/bin/python
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn import decomposition
import sklearn;
from PIL import Image
import glob;
import os;
import numpy as np;
from sklearn import svm;
import matplotlib.pyplot as plt

input_folder = "../processed_dataset/";
output_folder = "./output/";
input_files = glob.glob(input_folder + "*.jpg");


X = [];
Y = [];
for input_file in input_files:
  #c=0;
  im = Image.open(input_file);
  d = [];
  for p in im.getdata():
    d.append(p[1]);
    #c+=1;
  if os.path.splitext(input_file)[0].split('/')[-1][0] == 'm':
    Y.append(1);
  else:
    Y.append(-1);
  print "loaded " , input_file , len(d);
  X.append(d);

X = np.array(X);

print X.shape;

print "loaded " ,len(X), "files";

Y= np.asarray(Y);

sss = StratifiedShuffleSplit(n_splits=5, test_size=0.2);

accuracies=0;
for train_indices, test_indices in sss.split(X, Y):
  print "indices:", train_indices, test_indices;
  train_data, test_data = X[train_indices], X[test_indices];



  train_labels, test_labels = Y[train_indices], Y[test_indices];


  pca = decomposition.IncrementalPCA(n_components=60,whiten=True);
  pca.fit(train_data);
  print "COMPONENTS:", pca.components_;
  print "COMPONENTS SHAPE:", pca.components_.shape;
  print "explained_variance_ratio_", pca.explained_variance_ratio_;
  print "explained_variance_", pca.explained_variance_;
  print "explained_variance_ratio_ shape",  pca.explained_variance_ratio_.shape;
  print "COMPONENT CUMULATIVE VARIANCE";
  cumulative_variance = [];
  plot_ind = [];
  ind =0;
  sum_v = 0;
  for v in pca.explained_variance_ratio_:
    ind+=1;
    sum_v += v;
    cumulative_variance.append(sum_v);
    plot_ind.append(ind);
    print sum_v;
  plt.plot(plot_ind, cumulative_variance);
  # plt.show();
  train_data_reduced = pca.transform(train_data);
  test_data_reduced = pca.transform(test_data);

  clf = svm.SVC();
  clf.fit(train_data_reduced, train_labels);

  ind=0;
  count=0;
  male_correct = 0;
  female_correct = 0;
  male_incorrect = 0;
  female_incorrect = 0;
  for im in test_data_reduced:
    if clf.predict([im]) == test_labels[ind]:
      count+=1;
      if test_labels[ind] == 1:
        male_correct +=1;
      else:
        female_correct +=1;
    else:
      if test_labels[ind] == 1:
        male_incorrect +=1;
      else:
        female_incorrect +=1;
    ind+=1;

  accuracies += (count*1.0) / len(test_labels);
  print "accuracy", (count*1.0) / len(test_labels);
  print "male_correct: ", male_correct;
  print "female_correct: ", female_correct;
  print "male_incorrect: ", male_incorrect;
  print "female_incorrect: ", female_incorrect;
  print "================================================";

print "Mean accuracy:", accuracies/5;


"""
print "components_" , pca.components_;
for a in pca.components_:
  print "a:", a;
  print len(a);
  q = "";
  for b in a:
    q += b;
    q += " ";
  print q;
print "explained_variance_" , pca.explained_variance_;
print "explained_variance_ratio_", pca.explained_variance_ratio_;
print "singular_values_", pca.singular_values_;
print "mean_", pca.mean_;

print("done pca");
"""

"""
tot=0;
for val in pca.explained_variance_ratio_:
  tot += val;
  print tot;
"""
