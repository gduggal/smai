import tensorflow as tf
import sys
import os
# speicherorte fuer trainierten graph und labels in train.sh festlegen ##

# Disable tensorflow compilation warnings
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
import tensorflow as tf

import glob;
import numpy as np;

#input_files = glob.glob("./test/" + "*.jpg");
input_files = "./test/"
print('walk_dir (absolute) = ' + os.path.abspath(input_files))



X = [];
Y = [];
Z = [];
R = [];
E = []; 
f_correct = 0 ; 
f_incorrect = 0; 
m_correct = 0; 
m_incorrect = 0;
#for input_file in input_files:
for root, subdirs, files in os.walk(input_files):
   for filename in files:
     file_path = os.path.join(root, filename)
#     Y.append(os.path.split(root)[1])

#  if os.path.splitext(input_file)[0].split('/')[-1][0] == 'm':
#    Y.append('male');
#  else:
#    Y.append('female');
     X.append(os.path.normpath(file_path))
     image_path = os.path.normpath(file_path);
#image_path = sys.argv[1]
# angabe in console als argument nach dem aufruf  
     print(image_path);
     tf.global_variables_initializer()
     tf.reset_default_graph()
#bilddatei readen
     image_data = tf.gfile.FastGFile(image_path, 'rb').read()

# holt labels aus file in array 
     label_lines = [line.rstrip() for line 
                   in tf.gfile.GFile("tf_files/retrained_labels.txt")]
# !! labels befinden sich jeweils in eigenen lines -> keine aenderung in retrain.py noetig -> falsche darstellung im windows editor !!
				   
# graph einlesen, wurde in train.sh -> call retrain.py trainiert
     with tf.gfile.FastGFile("tf_files/retrained_graph.pb", 'rb') as f:
 
       graph_def = tf.GraphDef()	## The graph-graph_def is a saved copy of a TensorFlow graph; objektinitialisierung
       graph_def.ParseFromString(f.read())	#Parse serialized protocol buffer data into variable
       _ = tf.import_graph_def(graph_def, name='')	# import a serialized TensorFlow GraphDef protocol buffer, extract objects in the GraphDef as tf.Tensor
	
	#https://github.com/Hvass-Labs/TensorFlow-Tutorials/blob/master/inception.py ; ab zeile 276

     with tf.Session() as sess:

       softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
	# return: Tensor("final_result:0", shape=(?, 4), dtype=float32); stringname definiert in retrain.py, zeile 1064 

       predictions = sess.run(softmax_tensor, \
             {'DecodeJpeg/contents:0': image_data})
    # gibt prediction values in array zuerueck:
	
       top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
	# sortierung; circle -> 0, plus -> 1, square -> 2, triangle -> 3; array return bsp [3 1 2 0] -> sortiert nach groesster uebereinstimmmung
       Z.append(label_lines[top_k[0]])
       R.append(predictions[0][top_k[0]])
       if 'f' in filename:
           if  label_lines[top_k[0]] == 'female':
              f_correct +=1;
           else:
              f_incorrect +=1;
              E.append(filename) 
       if 'm' in filename: 
           if label_lines[top_k[0]] == 'male':
              m_correct +=1;
           else:
              m_incorrect +=1;
              E.append(filename)	
       # output
       for node_id in top_k:
          human_string = label_lines[node_id]
          score = predictions[0][node_id]
          print('%s (score = %.5f)' % (human_string, score))
             
   	#    Z.append(human_string)
     tf.Session().close()


#print( X )
#print ( Y )
#print ( Z)
#print (R)
print ('error images' , E)
print 'f_correct', f_correct , 'f_incorrect' , f_incorrect, 'm_correct', m_correct, 'm_incorrrect' , m_incorrect
