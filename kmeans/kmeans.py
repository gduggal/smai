from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.neighbors import NearestNeighbors
from sklearn import decomposition
import sklearn;
from PIL import Image
import glob;
import os;
import sys

import numpy as np;
#from sklearn import svm as kmeans;
from sklearn.cluster import KMeans

def print_image(filename, dataarr, size = (490,600)):
  image_data = "";
  for pixel in dataarr:
    if pixel < 0:
      pixel = 0.0;
    if pixel > 255:
      pixel = 255.0;
    image_data += chr(int(round(pixel))) + chr(int(round(pixel))) + chr(int(round(pixel)));
  im = Image.frombytes("RGB", size, image_data);
  im.save(filename, "PNG");

input_folder = sys.argv[1];#"./processed_dataset/";
output_folder = sys.argv[2]; #"./output/";
input_files = glob.glob(input_folder + sys.argv[3]);
gamma = sys.argv[4];
n_neighbor = 5;
M = [];
F = [];
X = [];
Y = [];
for input_file in input_files:
  #c=0;
  im = Image.open(input_file);
  d = [];
  for p in im.getdata():
    d.append(p[1]);
    #c+=1;
  if os.path.splitext(input_file)[0].split('/')[-1][0] == 'm':
    Y.append(1);
  else:
    Y.append(-1);
  print "loaded " , input_file , len(d);
  X.append(d);

X = np.array(X);

print X.shape;

print "loaded " ,len(X), "files";

Y= np.asarray(Y);

sss = StratifiedShuffleSplit(n_splits=5, test_size=0.2);

accuracies=0;
for train_indices, test_indices in sss.split(X, Y):
  test_data =  X[test_indices];
  test_labels =  Y[test_indices];
  train_data = X[train_indices];
  train_labels = Y[train_indices];
  print "indices:", train_indices, test_indices;
  ptr = 0;
  for d in train_data:
    if train_labels[ptr] == -1:
       F.append(d);
    else:
       M.append(d);
    ptr += 1;
  train_data_male = np.array(M);
  train_data_female = np.array(F);

  male_kmeans = KMeans(n_clusters=10,random_state=0)
  male_kmeans = male_kmeans.fit(train_data_male)
  centroids_male = male_kmeans.cluster_centers_

  female_kmeans = KMeans(n_clusters=10,random_state=0)
  female_kmeans = female_kmeans.fit(train_data_female)
  centroids_female = female_kmeans.cluster_centers_



  all_centroids = np.append(centroids_male, centroids_female, axis=0);
  # print "ALL CENTROIDS", all_centroids;
  ind =0;
  all_centroids_data = [];
  for centroid_count1 in range(4):
    for i in range(0, 600):
      for centroid_count2 in range(5):
        for j in range(0, 490):
          all_centroids_data.append(all_centroids[(centroid_count1 * 5) + centroid_count2][(i*490)+j]);
  print_image("./centroids/centroids.png", all_centroids_data, (490*5, 600*4));


  nbrs = NearestNeighbors(n_neighbors=n_neighbor, algorithm='kd_tree').fit(all_centroids)
  distances, indices = nbrs.kneighbors(test_data)
  print "DISTANCE", distances;
  print "INDICES", indices;
  
  ind=0;
  correct = 0;
  m_correct =0;
  m_incorrect =0;
  f_correct =0;
  f_incorrect =0;
  for im in test_data:
    print("neighbour indices:", indices[ind])
    male_ind_count=0;
    for indice in indices[ind]:
      if indice < 10:
         male_ind_count+=1;
    if male_ind_count < (1.0 * n_neighbor) / 2:
      if test_labels[ind] == -1:
        correct += 1;
        f_correct +=1;
      else:
        m_incorrect +=1;
    if male_ind_count > (1.0 * n_neighbor) / 2:
      if test_labels[ind] == 1:
        correct += 1;
        m_correct +=1;
      else:
        f_incorrect +=1;
    ind+=1;

  print "f_correct", f_correct;
  print "m_correct", m_correct
  print "f_incorrect", f_incorrect
  print "m_incorrect", m_incorrect
  accuracies += (correct*1.0) / len(test_labels);
  print "accuracy", (correct*1.0) / len(test_labels);
  print "================================================";

print "Mean accuracy:", accuracies/5;


