#!/usr/bin/python

from sklearn.model_selection import StratifiedShuffleSplit
import glob
import os
from PIL import Image
import numpy as np
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
import matplotlib.pyplot as plt

input_folder = "../processed_dataset/"
input_files = glob.glob(input_folder + "*.jpg")


X = []
Y = []

for input_file in input_files:
    im = Image.open(input_file)
    d = []
    for p in im.getdata():
        d.append(p[1])
    if os.path.splitext(input_file)[0].split('/')[-1][0] == 'm':
        Y.append(1)
    else:
        Y.append(-1)
    print "loaded ", input_file, len(d)
    X.append(d)

X = np.array(X)

print X.shape
print "loaded ", len(X), "files"

Y = np.asarray(Y)

n_splits = 5
test_size = 0.2
sss = StratifiedShuffleSplit(n_splits, test_size)
split_idx = 0

accuracy_list = []
male_accuracy_list = []
female_accuracy_list = []

for train_indices, test_indices in sss.split(X, Y):
    split_idx += 1
    train_data, test_data = X[train_indices], X[test_indices]
    train_labels, test_labels = Y[train_indices], Y[test_indices]

    lda_classifier = LDA("svd")
    lda_classifier.fit(train_data, train_labels)

    predictions = lda_classifier.predict(test_data)
    x_transform = lda_classifier.transform(train_data)

    color = lambda x: 'r' if x == -1 else 'b'
    mark = lambda x: 'o' if x == -1 else '^'

    fig = plt.figure('lda - ' + str(split_idx))
    ax = fig.add_subplot(111)

    t_index = 0
    for i in train_labels:
        ax.scatter(1, x_transform[t_index], c=color(i), marker=mark(i))
        t_index += 1

    accuracy = 0.0
    male_accuracy = 0.0
    female_accuracy = 0.0

    success = 0
    total_male_tests = 0
    total_female_tests = 0

    male_success = 0
    female_success = 0

    ind = 0
    for i in predictions:
        if i == test_labels[ind]:
            success += 1
            if test_labels[ind] == 1:
                male_success += 1
            else:
                female_success += 1

        if test_labels[ind] == 1:
            total_male_tests += 1
        else:
            total_female_tests += 1
        ind += 1

    accuracy = (success * 1.0)/len(test_labels)
    male_accuracy = (male_success * 1.0)/total_male_tests
    female_accuracy = (female_success * 1.0)/total_female_tests
    accuracy_list.append(accuracy)
    male_accuracy_list.append(male_accuracy)
    female_accuracy_list.append(female_accuracy)

print "Mean accuracy : ", sum(accuracy_list)/len(accuracy_list)
print "Mean male accuracy : ", sum(male_accuracy_list)/len(male_accuracy_list)
print "Mean female accuracy : ", sum(female_accuracy_list)/len(female_accuracy_list)  # noqa

plt.show()
