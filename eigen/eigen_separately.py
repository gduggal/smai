#!/usr/bin/python

from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.neighbors import NearestNeighbors
from sklearn import decomposition
from sklearn.preprocessing import StandardScaler
import sklearn
from PIL import Image
import glob
import os
import numpy as np
from sklearn import svm
import matplotlib.pyplot as plt
import matplotlib . cm as cm

input_folder = "../processed_dataset/"
input_files = glob.glob(input_folder + "*.jpg")
n_components = 5
n_splits = 1
test_size = 0.1


def normalize(X, low, high, dtype=None):
    X = np.asarray(X)
    minX, maxX = np.min(X), np.max(X)
    # normalize to [0...1].
    X = X-float(minX)
    X = X/float((maxX-minX))
    # scale to [ low ... high ].
    X = X*(high-low)
    X = X + low

    if dtype is None :
        return np.asarray(X)
    return np.asarray(X, dtype=dtype)


def print_image(filename, dataarr, size=(490, 600)):
    image_data = ""
    pixctr = 0
    pixctr255 = 0
    for pixel in dataarr:
        if pixel < 0:
            pixel = 0
            pixctr += 1
        if pixel > 255:
            pixctr255 += 1
            pixel = 255
        image_data += chr(int(round(pixel))) + chr(int(round(pixel))) + chr(int(round(pixel)))  # noqa
    im = Image.frombytes("RGB", size, image_data)
    im.save(filename, "PNG")


def preprocess(raw_train_data, g):
    #########SUBTRACTING MEAN#################
    scaler = StandardScaler(copy=True, with_mean=True, with_std=False)
    scaler.fit(raw_train_data)
    train_data = scaler.transform(raw_train_data)
    ##########################################

    ######Calculate Eigen vectors from train data########
    pca = decomposition.IncrementalPCA(n_components, whiten=True)
    pca.fit(train_data)

    train_data_reduced = pca.transform(train_data)

    count = 0

    for i in raw_train_data:
        r = train_data_reduced[count]
        print_image("output/" + g + "-" + str(count) + "rec.png", scaler.inverse_transform(pca.inverse_transform(r)))  # noqa
        count += 1

    # ecount = 0

    # eigens = [];
    # for e in pca.components_:
        # eigens.append(normalize(e, 0, 255, float))

    # all_eigens = [];
    # for ecount1 in range(4):
      # for i in range(0, 600):
        # for ecount2 in range(5):
          # for j in range(0, 490):
            # all_eigens.append(eigens[(ecount1 * 5) + ecount2][(i*490) + j]);
    # print_image("output/eigens.png", all_eigens, (490*5, 600*4));

    sum_v = 0
    for v in pca.explained_variance_ratio_:
        sum_v += v

    #####################################################

    #####Transforming from PCA#####
    ###############################
    return train_data_reduced, scaler, pca


def mean(ar):
    s = 0
    for v in ar:
        s += v
    return (s*1.0)/len(ar)

X = []
Y = []
Z = []

for input_file in input_files:
    im = Image.open(input_file)
    d = []
    for p in im.getdata():
        d.append(p[1])
    if os.path.splitext(input_file)[0].split('/')[-1][0] == 'm':
        Y.append(1)
    else:
        Y.append(-1)
    print "loaded ", input_file, len(d)
    X.append(d)
    Z.append(input_file)

X = np.array(X)

print X.shape
print "loaded ", len(X), "files"

Y = np.asarray(Y)

sss = StratifiedShuffleSplit(n_splits, test_size)

accuracies = 0
male_accuracies = 0
female_accuracies = 0

accuracies_list = []
male_accuracies_list = []
female_accuracies_list = []
raw_train_files = []

split_count = 0

for train_indices, test_indices in sss.split(X, Y):
    raw_train_data, raw_test_data = X[train_indices], X[test_indices]
    train_labels, test_labels = Y[train_indices], Y[test_indices]

    for i in train_indices:
        raw_train_files.append(Z[i])


    male_train_data = []
    female_train_data = []
    ind = 0

    for train_image in raw_train_data:
        if train_labels[ind] == 1:
            male_train_data.append(train_image)
        else:
            female_train_data.append(train_image)
        ind += 1

    male_data, male_scaler, male_pca = preprocess(male_train_data, 'male')
    female_data, female_scaler, female_pca = preprocess(female_train_data, 'female')

    test_data_male_scaler = male_scaler.transform(raw_test_data)
    test_data_male_projection = male_pca.transform(test_data_male_scaler)

    test_data_female_scaler = female_scaler.transform(raw_test_data)
    test_data_female_projection = female_pca.transform(test_data_female_scaler)

    ########Classifier#############
    male_nbrs = NearestNeighbors(n_neighbors=1, algorithm='kd_tree').fit(male_data)  # noqa
    male_distances, male_indices = male_nbrs.kneighbors(test_data_male_projection)  # noqa

    female_nbrs = NearestNeighbors(n_neighbors=1, algorithm='kd_tree').fit(female_data)  # noqa
    female_distances, female_indices = female_nbrs.kneighbors(test_data_female_projection)  # noqa

    ######Testing Model###############
    ind = 0
    count = 0

    total_test_males = 0
    total_test_females = 0

    incorrect_males = 0
    incorrect_females = 0

    male_x_axis = []
    male_to_male = []
    male_to_female = []

    female_x_axis = []
    female_to_male = []
    female_to_female = []

    for im in raw_test_data:
        mean_male_distance = mean(male_distances[ind])
        mean_female_distance = mean(female_distances[ind])

        if mean_male_distance < mean_female_distance:
            predict = 1
        else:
            predict = -1

        if test_labels[ind] == 1:
            male_x_axis.append(total_test_males)
            total_test_males += 1
            male_to_male.append(male_distances[ind][0])
            male_to_female.append(female_distances[ind][0])
        else:
            female_x_axis.append(total_test_females)
            total_test_females += 1
            female_to_male.append(male_distances[ind][0])
            female_to_female.append(female_distances[ind][0])

        if predict == test_labels[ind]:
            count += 1
        else:
            if test_labels[ind] == -1:
                incorrect_females += 1
            else:
                incorrect_males += 1

        ind += 1
    ###################################

    accuracies += (count*1.0) / len(test_labels)
    male_accuracies += (1.0 - (incorrect_males*1.0)/(total_test_males*1.0))
    female_accuracies += (1.0 - (incorrect_females*1.0)/(total_test_females*1.0))

    male_accuracies_list.append((1.0 - (incorrect_males*1.0)/(total_test_males*1.0)))
    female_accuracies_list.append((1.0 - (incorrect_females*1.0)/(total_test_females*1.0)))  # noqa
    accuracies_list.append(accuracies)

    split_count += 1
    plt.figure('male_' + str(split_count))
    plt.plot(male_x_axis, male_to_male, 'b', male_x_axis, male_to_female, 'r')

    plt.figure('female_' + str(split_count))
    plt.plot(female_x_axis, female_to_male, 'b', female_x_axis, female_to_female, 'r')  # noqa

print "Mean accuracy:", accuracies/(n_splits*1.0)
print "Mean male accuracy:", male_accuracies/(n_splits*1.0)
print "Mean female accuracy:", female_accuracies/(n_splits*1.0)

plt.show()

