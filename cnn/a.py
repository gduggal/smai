from PIL import Image;
import glob;
import os;

input_folder = "./training_dataset/";
output_folder = "./training_dataset/";

input_files = glob.glob(input_folder + "*.gif");
for gif_file in input_files:
  print "Converting ", gif_file;
  try:
    filepath, filename = os.path.split(gif_file);
    filtername,exts = os.path.splitext(filename);

    im = Image.open(gif_file).convert('RGB');

    im.save(output_folder + filtername + ".jpg");
  except Exception as exc:
    print "Error: " + str(exc)


