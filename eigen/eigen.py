#!/usr/bin/python
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.neighbors import NearestNeighbors
from sklearn import decomposition
from sklearn.preprocessing import StandardScaler
import sklearn;
from PIL import Image
import glob;
import os;
import numpy as np;
from sklearn import svm;

input_folder = "../processed_dataset/";
output_folder = "./output/";
input_files = glob.glob(input_folder + "*.jpg");


def get_majority(train_labels, indices):
  male_count=0;
  for ind in indices:
    if train_labels[ind] == 1:
      male_count += 1;
  if male_count < 3:
    return -1;
  return 1;

X = [];
Y = [];
for input_file in input_files:
  #c=0;
  im = Image.open(input_file);
  d = [];
  for p in im.getdata():
    d.append(p[1]);
    #c+=1;
  if os.path.splitext(input_file)[0].split('/')[-1][0] == 'm':
    Y.append(1);
  else:
    Y.append(-1);
  print "loaded " , input_file , len(d);
  X.append(d);

X = np.array(X);

print X.shape;

print "loaded " ,len(X), "files";

Y= np.asarray(Y);

sss = StratifiedShuffleSplit(n_splits=5, test_size=0.2);

accuracies=0;
for train_indices, test_indices in sss.split(X, Y):
  print "indices:", train_indices, test_indices;
  raw_train_data, raw_test_data = X[train_indices], X[test_indices];



  train_labels, test_labels = Y[train_indices], Y[test_indices];

  #####Calculate Mean from train data#####
  scaler = StandardScaler(copy=True, with_mean=True, with_std=False);
  scaler.fit(raw_train_data);
  print "MEAN FACE", scaler.mean_;
  ########Subtract Mean from all data######
  train_data = scaler.transform(raw_train_data);
  test_data = scaler.transform(raw_test_data); 
  ######################################


  ######Calculate Eigen vectors from train data########
  pca = decomposition.IncrementalPCA(n_components=50,whiten=True);
  pca.fit(train_data);
  print "COMPONENTS:", pca.components_;
  print "COMPONENTS SHAPE:", pca.components_.shape;
  print "explained_variance_ratio_", pca.explained_variance_ratio_;
  print "explained_variance_", pca.explained_variance_;
  print "explained_variance_ratio_ shape",  pca.explained_variance_ratio_.shape;
  print "COMPONENT CUMULATIVE VARIANCE";
  sum_v = 0;
  for v in pca.explained_variance_ratio_:
    sum_v += v;
    print sum_v; 
  #####################################################

  #####Transforming from PCA#####
  train_data_reduced = pca.transform(train_data);
  test_data_reduced = pca.transform(test_data);
  ###############################

  ########Classifier#############
  nbrs = NearestNeighbors(n_neighbors=5, algorithm='kd_tree').fit(train_data_reduced)
  distances, indices = nbrs.kneighbors(test_data_reduced)
  print "DISTANCE", distances;
  print "INDICES", indices;
  ###############################

  ######Testing Model###############
  ind=0;
  count=0;
  for im in test_data_reduced:
    print "checking for test_index:", ind;
    print "test_label" , test_labels[ind];
    print "Nearest neighbor", indices[ind];
    print "Neighbor label", train_labels[indices[ind]];
    if get_majority(train_labels, indices[ind]) == test_labels[ind]:
      print "CORRECT";
      count+=1;
    else:
      print "INCORRECT";
    ind+=1;
  ###################################

  accuracies += (count*1.0) / len(test_labels);
  print "accuracy", (count*1.0) / len(test_labels);
  print "================================================";

print "Mean accuracy:", accuracies/5;


"""
print "components_" , pca.components_;
for a in pca.components_:
  print "a:", a;
  print len(a);
  q = "";
  for b in a:
    q += b;
    q += " ";
  print q;
print "explained_variance_" , pca.explained_variance_;
print "explained_variance_ratio_", pca.explained_variance_ratio_;
print "singular_values_", pca.singular_values_;
print "mean_", pca.mean_;

print("done pca");
"""

"""
tot=0;
for val in pca.explained_variance_ratio_:
  tot += val;
  print tot;
"""
